function [err,Derr] = errorNorm(T,u,Du,eta)
% Calculates the error of a finite element solution
% SYNOPSIS:
%  [err, Derr] =  errorNorm(G, sol, Dsol, eta)
%
% Parameters:
%   T     Triangulation
%   sol   True solution
%   Dsol  Derivative of true solution
%   eta   finte element solution
%
% Returns:
%   err   error in L_2 norm
%   Derr  error in a-norm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright Runar Berge: runar.berge@uib.no
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


uh   = @(x) griddata(T.coordinates(:,1), T.coordinates(:,2), eta, x(:,1), x(:,2));
intF = @(x) sum((u(x) - uh(x)).^2,2);
err  = sqrt(triangleInt(T,intF));

Derr = zeros(size(T.elements,1),1);
for i = 1:size(T.elements,1)
  X = T.coordinates(T.elements(i,:),:);
  K1 = det([X(2,:)-X(1,:); X(3,:) - X(1,:)]);
  K2 = det([X(1,:)-X(2,:); X(3,:) - X(2,:)]);
  K3 = det([X(2,:)-X(3,:); X(1,:) - X(3,:)]);
  Gphi1 = [X(2,2) - X(3,2); X(3,1) - X(2,1)]/K1;
  Gphi2 = [X(1,2) - X(3,2); X(3,1) - X(1,1)]/K2;
  Gphi3 = [X(2,2) - X(1,2); X(1,1) - X(2,1)]/K3;
  Gphi = sum(bsxfun(@times, eta(T.elements(i,:)),[Gphi1';Gphi2';Gphi3']),1);
  
  g = @(x) sum(bsxfun(@minus,Du(x), Gphi).^2,2);
  Tloc.coordinates = X;
  Tloc.elements = [1,2,3];
  Derr(i) = triangleInt(Tloc, g);
end
Derr = sqrt(sum(Derr));
end