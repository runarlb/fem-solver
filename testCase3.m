clear all; close all
%% solution u = sin(2pi*x)*cos(2pi*x) on the unit square with nauman on bottom bndr
f = @(x) 8*pi^2*sin(2*pi*x(:,1)).*cos(2*pi*x(:,2)); % Right hand side
u_d = @(x) sin(2*pi*x(:,1)).*cos(2*pi*x(:,2));  % Analytic solution

Du_d = @(x) [2*pi*cos(2*pi*x(:,1)).*cos(2*pi*x(:,2)), ... % gradient of solution
            -2*pi*sin(2*pi*x(:,1)).*sin(2*pi*x(:,2))];
g = @(x) 2*pi*sin(2*pi*x(:,1)).*sin(2*pi*x(:,2)); % Du_d'*[0,-1]

bnd = [0,0;0,2;2,2;2,1;1,1;1,0];      % Set boundary

del = [bnd(1:3,:),     bnd(2:4,:); ...
       bnd(5:end-1,:), bnd(6:end,:)]; % Delaunay boundary
neu = [bnd(4,:),       bnd(5,:); ...  
       bnd(end,:),     bnd(1,:)];     % Neuman boundary

n = 3;

err = zeros(n,1);
Derr = zeros(n,1);
h = zeros(n,1);

TOL = 1e-10;
for m = 1:n
  [X,Y] = meshgrid(linspace(0,2,1+2^(m+4))); % Create vertexes
  pts = [X(:),Y(:)];
  rem = pts(:,1)>1 & pts(:,2) < 1;           % Find vertexes outside boundary
  pts = pts(~rem,:);                         % And remove them
  T = triangulationMapping(pts,del,neu);     % Create triangulation

  t = T.elements;
  pmid = (pts(t(:,1),:)+pts(t(:,2),:)+pts(t(:,3),:))/3;% Compute edge centroids
  T.elements = t(pmid(:,1)<1 | pmid(:,2)>1,:);         % Keep interior triangles

  uh = fem2d(T,f,u_d,g);                     % Compute FEM Solution
  
  [err(m),Derr(m)] = errorNorm(T,u_d,Du_d, uh); % Calculate error
  h(m) = max(sqrt(sum((circCenter(T) - ...      % Calculate larges cell
                       T.coordinates(T.elements(:,1),:)).^2,2)));
end

%% Load data 
load('testCase3.mat');

%% Plot
close all
set(0,'DefaultTextInterpreter','none');
% plot domain
[X,Y] = meshgrid(linspace(0,2,1+2^(1+3))); % Create vertexes
pts = [X(:),Y(:)];
rem = pts(:,1)>1 & pts(:,2) < 1;           % Find vertexes outside boundary
pts = pts(~rem,:);                         % And remove them
Tp = triangulationMapping(pts,del,neu);     % Create triangulation
t = Tp.elements;
pmid = (pts(t(:,1),:)+pts(t(:,2),:)+pts(t(:,3),:))/3;% Compute edge centroids
Tp.elements = t(pmid(:,1)<1 | pmid(:,2)>1,:);         % Keep interior triangles

figure(1)
hold on
patch('vertices',Tp.coordinates, 'faces', Tp.elements, 'facealpha',0)
for i = 1:size(neu,1);
plot([neu(i,1),neu(i,3)],[neu(i,2),neu(i,4)], 'r','linewidth',2)
end
axis equal tight

% Plot contourplot
figure(2)
trisurf(T.elements, T.coordinates(:,1), T.coordinates(:,2),uh,'edgealpha',0)
h1 = colorbar();
ylabel(h1,'$u_h$','Interpreter','latex','fontsize',20)
view(0,90)
axis equal tight off
figure(3)
trisurf(T.elements, T.coordinates(:,1), T.coordinates(:,2),abs(uh-u_d(T.coordinates)),'edgealpha',0)
h2 = colorbar();
ylabel(h2,'$|u_h-u|$','Interpreter','latex','fontsize',20);
view(0,90)
axis equal tight off
% Plot convergence
plotFemError(h,err,Derr)
title('$u(x,y) = \sin(2\pi x)\cos(2\pi y)$');
ylabel('$\ln(\text{error})$')
xlabel('$\ln(h)$')


%% Save figure
addpath matlab2tikz/src
figure(1)
path = '../tex/fig/';
print(strcat(path,'testCase3Grid'),'-depsc','-painters')

figure(2)
path = '../tex/fig/';
print(strcat(path,'testCase3Sol'),'-depsc','-painters');

figure(3)
path = '../tex/fig/';
print(strcat(path,'testCase3err'),'-depsc','-painters');

figure(4)
path = '../tex/fig/testCase3.tex';
matlab2tikz(path,'showinfo',false,'parseStrings',false, ...
                 'width','\figW','height','\figH')
%% Save
%close all
%save('testCase3')