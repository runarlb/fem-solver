function h = plotFemRes(u, T)
% Graphical view of the computed solution
h = trisurf(T.elements, T.coordinates(:,1), T.coordinates(:,2), u','facecolor','interp');
view(10,40);
end