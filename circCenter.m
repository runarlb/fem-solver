function cc = circCenter(T)

AB = (T.coordinates(T.elements(:,1),:) + T.coordinates(T.elements(:,2),:))/2;
AC = (T.coordinates(T.elements(:,1),:) + T.coordinates(T.elements(:,3),:))/2;
Tab= AB - T.coordinates(T.elements(:,1),:);
Tac= AC - T.coordinates(T.elements(:,1),:);

ABdotTab = sum(AB.*Tab,2);
ACdotTac = sum(AC.*Tac,2);
cc = zeros(size(T.elements,1),2);
for i = 1:size(T.elements,1)
   A = [Tab(i,:); Tac(i,:)];
   b = [ABdotTab(i);ACdotTac(i)];
   cc(i,:) = A\b;
end


end