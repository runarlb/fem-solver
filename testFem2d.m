pts = rand(20,2);
bnd =[0,0;0,1;1,1;1,0];
f = @(x) zeros(size(x,1),1);
g = @(x) sqrt(sum(x.^2,2));
u_d = @(x) sqrt(sum(x.^2,2));

pts = [bnd;pts];

neu = [bnd(1:end-1,:), bnd(2:end,:)];
del = [bnd(end,:), bnd(1,:)];
T = triangulationMapping(pts,del,neu);

u = fem2d(T,f,u_d,g);

plotFemRes(u,T);