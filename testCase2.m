clear all; close all
%% solution u = exp(x)sin(x) on the unit square with nauman on bottom bndr
f = @(x) -2*exp(x(:,1)).*cos(x(:,1)); % Right hand side
u_d = @(x) exp(x(:,1)).*sin(x(:,1));  % Analytic solution
Du_d = @(x) [exp(x(:,1)).*sin(x(:,1)) + exp(x(:,1)).*cos(x(:,1)), zeros(size(x(:,1)))];
g = @(x) exp(x(:,1)).*sin(x(:,1)) + exp(x(:,1)).*cos(x(:,1)); % bottom boundary.

bnd = [0,0;0,1;1,1;1,0];
del = [bnd(1:2,:), bnd(2:3,:);...
       bnd(4,:),   bnd(1,:)];

neu = [bnd(3,:), bnd(4,:)];

n = 3;

err = zeros(n,1);
Derr = zeros(n,1);
h = zeros(n,1);

for m = 1:n
  [X,Y] = meshgrid(linspace(0,1,2^(m+2)));
  pts = [X(:),Y(:)];
  T = triangulationMapping(pts,del,neu);
  uh = fem2d(T,f,u_d,g);
  
  [err(m),Derr(m)] = errorNorm(T,u_d,Du_d, uh);
  h(m) = max(sqrt(sum((circCenter(T) - T.coordinates(T.elements(:,1),:)).^2,2)));
end


%% Plot
close all
set(0,'DefaultTextInterpreter','none');
plotFemError(h,err,Derr)
title('$u(x,y) = e^x sin(x)$');
ylabel('$\ln(\text{error})$')
xlabel('$\ln(h)$')


%% Save figure
addpath matlab2tikz/src
path = '../tex/fig/testCase2.tex';
matlab2tikz(path,'showinfo',false,'parseStrings',false, ...
                 'width','\figW','height','\figH')