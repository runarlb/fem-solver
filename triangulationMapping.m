function [T] =triangulationMapping(pts, del, neu)

% Create triangulation
T.elements = delaunay(pts);
T.coordinates = pts;

% Find the edges
E = reshape(T.elements(:,[1,2,2,3,3,1])',2,[])';

% Locate edges on dirichlet boundary
Id1 = isOnLineSegment(pts(E(:,1),:), del);
Id2 = isOnLineSegment(pts(E(:,2),:), del);
Id  = any(Id1 & Id2,2);
T.dirichlet = E(Id,:);
% Locate edges on neumann boundary
In1 = isOnLineSegment(pts(E(:,1),:), neu);
In2 = isOnLineSegment(pts(E(:,2),:), neu);
In  = any(In1 & In2,2);
T.neumann = E(In,:);
end


function I = isOnLineSegment(p,L)
TOL = 100*eps;
if isempty(L)
  I = false(size(p,1),1);
  return
end

% create distance func
eucDist = @(p) sqrt(p(:,1:2:end).^2 + p(:,2:2:end).^2);

n = size(L,1);
p = repmat(p,1,n);
l1 = reshape(L(:,1:2)',1,[]);
l2 = reshape(L(:,3:4)',1,[]);
a = bsxfun(@minus, p, l1);
b = bsxfun(@minus, p, l2);

err = bsxfun(@minus, eucDist(a) + eucDist(b), eucDist(L(:,1:2) - L(:,3:4))');
I = abs(err) < TOL;
end