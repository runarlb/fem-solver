function [I] = triangleInt(T, f)

x = T.coordinates;
[Xq, w, ~, vol] = triangleQuadRule(7);
I = 0;
for i = 1:size(T.elements,1)
  x = T.coordinates(T.elements(i,:),:);
  P = @(eta)  bsxfun(@times, x(1,:), 1-eta(:,1) - eta(:,2)) ...
            + bsxfun(@times, x(2,:), eta(:,1)) ...
            + bsxfun(@times, x(3,:), eta(:,2));
  A2 = abs(x(:,1)'*[x(2,2) - x(3,2); x(3,2) - x(1,2); x(1,2) - x(2,2)]);
  I = I + A2*vol*w*f(P(Xq));
end




end