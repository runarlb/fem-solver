function u = fem2d(T,f,u_d,g)
% FEM2D 2D finite element method (FEM) for the Laplacian
% Initialization
elements = T.elements;
coordinates = T.coordinates;
dirichlet = T.dirichlet;
neumann = T.neumann;

number_of_elements = size(elements, 1);
number_of_nodes = size(coordinates, 1);
A = sparse(number_of_nodes, number_of_nodes);
b = zeros(number_of_nodes, 1);
u = zeros(number_of_nodes, 1);
% Assembly of the stiffness matrix and volume forces
for k = 1:number_of_elements
nodes = elements(k,:);
vertices = coordinates(nodes,:);
E = vertices([2 3 1],:) - vertices([3 1 2],:);
area = abs(E(2,1)*E(3,2) - E(3,1)*E(2,2))/2;
A(nodes,nodes) = A(nodes,nodes) + 1/(4*area) * E*E'; 
b(nodes) = b(nodes) + area/3 * f(sum(vertices)/3);
end
% Assembly of the Neumann conditions
for j = 1:size(neumann,1)
nodes = neumann(j,:);
vertices = coordinates(nodes,:);
E = vertices(2,:) - vertices(1,:);
b(nodes) = b(nodes) + norm(E)/2 * g(sum(vertices)/2);
end
% Incorporation of the Dirichlet conditions
DirichletNodes = unique(dirichlet);
u(DirichletNodes) = u_d(coordinates(DirichletNodes,:));

% Solving the system of linear equations
FreeNodes = setdiff(1:number_of_nodes, DirichletNodes);
u(FreeNodes) = A(FreeNodes, FreeNodes) \ ...
(b(FreeNodes) - A(FreeNodes, DirichletNodes) * u(DirichletNodes));
end