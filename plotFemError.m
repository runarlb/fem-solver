function F =  plotFemError(h,err,Derr)
F = figure();
hold on
%loglog(h,err);
%loglog(h,Derr);
plot(log(h),log(err));
plot(log(h),log(Derr));
a = polyfit(log(h),log(err),1);
b = polyfit(log(h),log(Derr),1);
legend(['$||u - u_h||_{L_2},\ \text{Slope:}\ ' num2str(a(1)) '$'] , ...
       ['$||u - u_h||_a ,\ \text{Slope:}\ ' num2str(b(1)) '$'],'location','southEast');
end